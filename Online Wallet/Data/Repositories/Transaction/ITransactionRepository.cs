﻿using Online_Wallet.Data.Interfaces;

namespace Online_Wallet.Data.Repositories.Transaction
{
    public interface ITransactionRepository: IRepository<Entities.Transaction>
    {
       
    }
}