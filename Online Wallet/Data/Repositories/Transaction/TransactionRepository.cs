﻿using Online_Wallet.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Online_Wallet.Data.Repositories.Transaction
{
    public class TransactionRepository: Repository<Entities.Transaction>, ITransactionRepository
    {
        public TransactionRepository(ApplicationDbContext context) : base(context)
        {
        }


    }
}
