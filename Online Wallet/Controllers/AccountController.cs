﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Online_Wallet.Data.Factories;
using Online_Wallet.Models;

namespace Online_Wallet.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        private readonly UserManager<User> _userManager;
         public AccountController(UserManager<User> userManager, IUnitOfWorkFactory unitofWorkFactory)
        {
            _userManager = userManager;
            _unitOfWorkFactory = unitofWorkFactory;
        }
        [HttpPost]
        public async Task<IActionResult> AddMountToAccount(int mount, int toUser)
        {
            int mountOutput;
            int userOutput;
            if (int.TryParse(mount.ToString(), out mountOutput) && int.TryParse(toUser.ToString(), out userOutput))
            {
                using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
                {
                    var user = await _userManager.FindByNameAsync(userOutput.ToString());
                    if(user != null)
                    {
                        user.Balance += mountOutput;
                        Transaction transaction = new Transaction
                        {
                            Amount = mountOutput.ToString(),
                            Description = "Пополнение",
                            FromUser = "Anonim",
                            ToUser = user.Email,
                            TransactonDate = DateTime.Now,
                            UserId = user.Id
                        };
                        await _userManager.UpdateAsync(user);
                        await unitOfWork.Transactions.CreateAsync(transaction);
                        await unitOfWork.CompleteAsync();
                    }
                    else
                    {
                        return View("Error", new ErrorViewModel { RequestId = "Пользователь не найден" });
                    }
                } 
            }
            else
            {
                return View("Error", new ErrorViewModel { RequestId = "Введено не число" });
            }
            return RedirectToAction("Index", "Home");
        }
        public ActionResult SendMoney()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> SendMoneyToUser(string toUser, int mount)
        {
            int ouputMount;
            int outputUserNumber;
            if (int.TryParse(mount.ToString(), out ouputMount) && int.TryParse(toUser, out outputUserNumber))
            {
                using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
                {
                    
                    var authUser = await _userManager.GetUserAsync(User);
                    var user = await _userManager.FindByNameAsync(outputUserNumber.ToString());
                    if (user != null && authUser.Balance >= ouputMount)
                    {
                        user.Balance += ouputMount;
                        authUser.Balance -= ouputMount;
                        Transaction replenishmentTransaction = new Transaction
                        {
                            Amount = ouputMount.ToString(),
                            Description = "Пополнение",
                            FromUser = authUser.Email,
                            ToUser = user.Email,
                            TransactonDate = DateTime.Now,
                            UserId = user.Id
                        };
                        Transaction transferTransaction = new Transaction
                        {
                            Amount = ouputMount.ToString(),
                            Description = "Перевод",
                            FromUser = user.Email,
                            ToUser = authUser.Email,
                            TransactonDate = DateTime.Now,
                            UserId = authUser.Id
                        };
                        await unitOfWork.Transactions.CreateAsync(replenishmentTransaction);
                        await unitOfWork.Transactions.CreateAsync(transferTransaction);
                        await unitOfWork.CompleteAsync();
                        await _userManager.UpdateAsync(user);
                        await _userManager.UpdateAsync(authUser);
                    }
                    return RedirectToAction("Index", "Home");
                }
            }
            return View("Error", new ErrorViewModel {RequestId = "Пользователь не найден или недостачно средств на балансе" });

        }

    }
}