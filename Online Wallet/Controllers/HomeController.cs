﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Online_Wallet.Data.Factories;
using Online_Wallet.Models;

namespace Online_Wallet.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly UserManager<User> _userManager;
        public HomeController(IUnitOfWorkFactory unitofWorkFactory, UserManager<User> userManager)
        {
            _unitOfWorkFactory = unitofWorkFactory;
            _userManager = userManager;
        }
        public async Task<IActionResult> Index()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (User.Identity.IsAuthenticated)
                {
                    var user = await _userManager.FindByEmailAsync(User.Identity.Name);
                    var transactions = unitOfWork.Transactions.GetAllAsync().Result.Where(c => c.UserId == user.Id);
                    return View(transactions.ToList());
                }
                return View();
            }
            
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
