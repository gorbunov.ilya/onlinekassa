﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
   public class Transaction : BaseEntity
   {
    public string ToUser { get; set; }
    public string FromUser { get; set; }
    public string Amount { get; set; }
    public DateTime TransactonDate { get; set; }
    public string Description { get; set; }
    public User User { get; set; }
    public string UserId { get; set; }
    }
}
