﻿using Microsoft.EntityFrameworkCore;
using Online_Wallet.Data;

namespace Data.Factories
{
    public interface IApplicationDbContextFactory
    {
        ApplicationDbContext CreateContext();
    }

    public class ApplicationDbContextFactory : IApplicationDbContextFactory
    {
        private readonly DbContextOptions<ApplicationDbContext> _options;

        public ApplicationDbContextFactory(
            DbContextOptions<ApplicationDbContext> options)
        {
            _options = options;
        }

        public ApplicationDbContext CreateContext()
        {
            return new ApplicationDbContext(_options);
        }
    }
}
