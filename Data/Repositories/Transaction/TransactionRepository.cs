﻿using System;
using System.Collections.Generic;
using System.Text;
using Entities;
using Online_Wallet.Data;

namespace Data.Repositories.Transaction
{
    class TransactionRepository : Repository<Entities.Transaction>, ITransactionRepository
    {
        public TransactionRepository(ApplicationDbContext context) : base(context)
        {
        }

    }
}
