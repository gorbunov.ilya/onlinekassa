﻿using Data.Interfaces;
using Entities;
namespace Data.Repositories.Transaction
{
    public interface ITransactionRepository : IRepository<Entities.Transaction>
    {
    }
}