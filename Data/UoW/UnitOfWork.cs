﻿using Data.Interfaces;
using Data.Repositories;
using Data.Repositories.Transaction;
using Entities;
using Online_Wallet.Data;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Data.UoW
{
    class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        private readonly ConcurrentDictionary<Type, object> _repositories;

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
            _repositories = new ConcurrentDictionary<Type, object>();

            Transactions = new TransactionRepository(context);
        }

        public ITransactionRepository Transactions { get; }

        public Task<int> CompleteAsync()
        {
            return _context.SaveChangesAsync();
        }

        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : BaseEntity
        {
            return _repositories.GetOrAdd(typeof(TEntity), new Repository<TEntity>(_context)) as IRepository<TEntity>;
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}

